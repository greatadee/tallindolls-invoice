<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerQES710r\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerQES710r/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerQES710r.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerQES710r\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerQES710r\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'QES710r',
    'container.build_id' => 'a63fe192',
    'container.build_time' => 1630959032,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerQES710r');
