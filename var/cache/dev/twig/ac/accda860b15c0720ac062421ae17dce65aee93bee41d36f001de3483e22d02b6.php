<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* invoices/index.html.twig */
class __TwigTemplate_58c042cf7aeec9da012ef720fa5adc59c7a6e7aaecacee863c3b7029d1c27172 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "invoices/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "invoices/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "invoices/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    <style>
        .pagination {
            margin:0 auto;
            justify-content: center;
        }
        .pagination span {
            margin: 0 10px;
        }
        .pagination span a, .pagination span.current{
            display: flex;
            width: 35px;
            height: 35px;
            align-items: center;
            justify-content: center;
            color: #343a40;
            transition: 0.3s ease-in-out;
        }
        .pagination span.current{
            background: #343a40;
            color: #fff;
        }
        .pagination span a {
            border: #343a40 solid 1px;
        }
        .pagination span a:hover{
            background: #343a40;
            color: #fff;
            text-decoration: none;
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 34
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 35
        echo "    <div class=\"container\">
        <div class=\"row py-5\">
            <div class=\"col-md-6\">
                <h1>Invoice Stats:</h1>
            </div>
            <div class=\"col-md-6 text-right\">
                <h2>Total Amount: <span>";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["totalInvoicesAmount"]) || array_key_exists("totalInvoicesAmount", $context) ? $context["totalInvoicesAmount"] : (function () { throw new RuntimeError('Variable "totalInvoicesAmount" does not exist.', 41, $this->source); })()), "html", null, true);
        echo "</span></h2>
            </div>
        </div>

        <div class=\"row d-flex align-items-center mb-5\">
            <div class=\"col-md-4\">
                <h1>Invoices:</h1>
            </div>
            <div class=\"col-md-8 text-right\">
                <a class=\"btn btn-secondary\" href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("invoices.create");
        echo "\">Upload Invoice</a>
                <a class=\"btn btn-info\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("invoice.logs.index");
        echo "\">View Logs</a>
            </div>
        </div>

        <div class=\"row\">

            ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 57, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 57));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 58
            echo "                <div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
                    <strong>Success!</strong> ";
            // line 59
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "            <table class=\"table table-dark\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Amount</th>
                        <th>Selling Price</th>
                        <th>Due on</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    ";
        // line 76
        if ( !twig_test_empty((isset($context["invoices"]) || array_key_exists("invoices", $context) ? $context["invoices"] : (function () { throw new RuntimeError('Variable "invoices" does not exist.', 76, $this->source); })()))) {
            // line 77
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["invoices"]) || array_key_exists("invoices", $context) ? $context["invoices"] : (function () { throw new RuntimeError('Variable "invoices" does not exist.', 77, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
                // line 78
                echo "                            <tr>
                                <td>";
                // line 79
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "id", [], "array", false, false, false, 79), "html", null, true);
                echo "</td>
                                <td>";
                // line 80
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "amount", [], "array", false, false, false, 80), "html", null, true);
                echo "</td>
                                <td>";
                // line 81
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "selling_price", [], "array", false, false, false, 81), "html", null, true);
                echo "</td>
                                <td>";
                // line 82
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "due_on", [], "array", false, false, false, 82), "Y-m-d"), "html", null, true);
                echo "</td>
                                <td>";
                // line 83
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["invoice"], "created_at", [], "array", false, false, false, 83), "Y-m-d H:i:s"), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "                    ";
        }
        // line 87
        echo "                </tbody>
            </table>

        </div>
        <div class=\"row\">
            <div class=\"col-md-12 text-center\">
                ";
        // line 93
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["invoices"]) || array_key_exists("invoices", $context) ? $context["invoices"] : (function () { throw new RuntimeError('Variable "invoices" does not exist.', 93, $this->source); })()));
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "invoices/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 93,  222 => 87,  219 => 86,  210 => 83,  206 => 82,  202 => 81,  198 => 80,  194 => 79,  191 => 78,  186 => 77,  184 => 76,  171 => 65,  159 => 59,  156 => 58,  152 => 57,  143 => 51,  139 => 50,  127 => 41,  119 => 35,  109 => 34,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block stylesheets %}
    <style>
        .pagination {
            margin:0 auto;
            justify-content: center;
        }
        .pagination span {
            margin: 0 10px;
        }
        .pagination span a, .pagination span.current{
            display: flex;
            width: 35px;
            height: 35px;
            align-items: center;
            justify-content: center;
            color: #343a40;
            transition: 0.3s ease-in-out;
        }
        .pagination span.current{
            background: #343a40;
            color: #fff;
        }
        .pagination span a {
            border: #343a40 solid 1px;
        }
        .pagination span a:hover{
            background: #343a40;
            color: #fff;
            text-decoration: none;
        }
    </style>
{% endblock %}
{% block body %}
    <div class=\"container\">
        <div class=\"row py-5\">
            <div class=\"col-md-6\">
                <h1>Invoice Stats:</h1>
            </div>
            <div class=\"col-md-6 text-right\">
                <h2>Total Amount: <span>{{ totalInvoicesAmount }}</span></h2>
            </div>
        </div>

        <div class=\"row d-flex align-items-center mb-5\">
            <div class=\"col-md-4\">
                <h1>Invoices:</h1>
            </div>
            <div class=\"col-md-8 text-right\">
                <a class=\"btn btn-secondary\" href=\"{{ path('invoices.create') }}\">Upload Invoice</a>
                <a class=\"btn btn-info\" href=\"{{ path('invoice.logs.index') }}\">View Logs</a>
            </div>
        </div>

        <div class=\"row\">

            {% for message in app.flashes('success')  %}
                <div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
                    <strong>Success!</strong> {{ message }}
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
            {% endfor %}
            <table class=\"table table-dark\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Amount</th>
                        <th>Selling Price</th>
                        <th>Due on</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    {% if invoices is not empty %}
                        {% for invoice in invoices %}
                            <tr>
                                <td>{{ invoice['id'] }}</td>
                                <td>{{ invoice['amount'] }}</td>
                                <td>{{ invoice['selling_price'] }}</td>
                                <td>{{ invoice['due_on']|date('Y-m-d') }}</td>
                                <td>{{ invoice['created_at']|date('Y-m-d H:i:s') }}</td>
                            </tr>
                        {% endfor %}
                    {% endif %}
                </tbody>
            </table>

        </div>
        <div class=\"row\">
            <div class=\"col-md-12 text-center\">
                {{ knp_pagination_render(invoices) }}
            </div>
        </div>
    </div>
{% endblock %}", "invoices/index.html.twig", "/home/coeus/Desktop/My-Projects/tallinndolls/templates/invoices/index.html.twig");
    }
}
