<?php

namespace App\Service;

use App\Entity\Invoice;
use App\Enum\CommonEnum;
use App\Repository\InvoiceRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class InvoiceService
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var InvoiceLogService
     */
    private $invoiceLogService;

    /**
     * @param InvoiceRepository $invoiceRepository
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        InvoiceRepository     $invoiceRepository,
        ParameterBagInterface $parameterBag,
        InvoiceLogService     $invoiceLogService
    )
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->parameterBag = $parameterBag;
        $this->invoiceLogService = $invoiceLogService;
    }

    /**
     * @return int|mixed|string
     */
    public function getAllInvoices()
    {
        return $this->invoiceRepository->getAllInvoices();
    }

    /**
     * @return int|mixed|string
     */
    public function getTotalInvoicesAmount()
    {
        return $this->invoiceRepository->getTotalInvoicesAmount();
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function uploadCSVFile(UploadedFile $file)
    {
        $uploadDirectory = $this->parameterBag->get('uploadDirectory');
        $fileName = time() . $file->getClientOriginalName();

        $file->move(
            $uploadDirectory,
            $fileName
        );

        return $fileName;
    }

    /**
     * @param array $data
     * @param $row
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveInvoice(array $data, int $row, string $fileName)
    {
        //if csv row data is valid then add data in database
        if ($this->validateInvoiceRow($data, $row, $fileName)) {
            $this->invoiceRepository->create($data);
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function validateInvoiceRow(array $data, int $row, string $fileName): bool
    {
        if (empty($data['id']) || empty($data['amount'] || empty($data['due on']))) {
            //invalid row data
            $this->createInvoiceLog(['file_name' => $fileName, 'message' => "Id, amount or due on is empty on row {$row}"]);
            return false;
        }

        if (!self::isNumeric($data['id'])) {
            //id data is not numeric
            $this->createInvoiceLog(['file_name' => $fileName, 'message' => "Id is not numeric on row {$row}"]);
            return false;
        }

        if (!self::isNumeric($data['amount'])) {
            //amount is not numeric
            $this->createInvoiceLog(['file_name' => $fileName, 'message' => "Amount is not numeric on row {$row}"]);
            return false;
        }

        if (!self::isDate($data['due on'])) {
            //due on is not a date
            $this->createInvoiceLog(['file_name' => $fileName, 'message' => "Date is not correct on row {$row}"]);

            return false;
        }

        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    private static function isNumeric($value): bool
    {
        return is_numeric($value);
    }

    /**
     * @param $value
     * @return bool
     */
    private static function isDate($value): bool
    {
        return strtotime($value);
    }

    private function createInvoiceLog(array $data)
    {
        $this->invoiceLogService->create($data);
    }

    /**
     * @param Invoice $invoice
     * @return float|int|string|null
     */
    public function calculateSellingPrice(Invoice $invoice)
    {
        $sellingPrice = $invoice->getAmount();
        if (!empty($invoice->getDueOn())) {
            $differenceInDays = self::calculateDaysBetweenDays($invoice->getDueOn(), $invoice->getCreated());
            if ($differenceInDays <= CommonEnum::COEFFICIENT_DAYS) {
                $sellingPrice = $invoice->getAmount() * CommonEnum::COEFFICIENT_BEFORE_30_DAYS;
            } else {
                $sellingPrice = $invoice->getAmount() * CommonEnum::COEFFICIENT_AFTER_30_DAYS;
            }
        }

        return $sellingPrice;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return int
     */
    private static function calculateDaysBetweenDays($startDate, $endDate): int
    {
        return (int)$startDate->diff($endDate)->format('%a');
    }
}