<?php

namespace App\Service;

use App\Entity\InvoiceLog;
use App\Repository\InvoiceLogRepository;

class InvoiceLogService
{
    /**
     * @var InvoiceLogRepository
     */
    private $invoiceLogRepository;

    /**
     * @param InvoiceLogRepository $invoiceLogRepository
     */
    public function __construct(InvoiceLogRepository $invoiceLogRepository)
    {
        $this->invoiceLogRepository = $invoiceLogRepository;
    }

    /**
     * @return array|object[]
     */
    public function getAllInvoiceLogs()
    {
        return $this->invoiceLogRepository->getAllInvoiceLogs();
    }

    /**
     * @param array $data
     * @return \App\Entity\InvoiceLog
     */
    public function create(array $data): InvoiceLog
    {
        return $this->invoiceLogRepository->create($data);
    }
}