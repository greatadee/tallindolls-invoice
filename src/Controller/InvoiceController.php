<?php

namespace App\Controller;

use App\Enum\CommonEnum;
use App\Service\InvoiceService;
use Knp\Component\Pager\PaginatorInterface;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    /**
     * @Route(path="/invoices", name="invoices.index")
     * @param Request $request
     * @param InvoiceService $invoiceService
     * @param PaginatorInterface $paginator
     */
    public function index(
        Request $request,
        InvoiceService $invoiceService,
        PaginatorInterface $paginator
    ): \Symfony\Component\HttpFoundation\Response
    {
        $perPage = $request->query->get('page', CommonEnum::DEFAULT_PAGE);
        $invoices = $invoiceService->getAllInvoices();
        $data = [];
        foreach ($invoices as $invoice) {
            $data[] = [
                'id' => $invoice->getId(),
                'amount' => $invoice->getAmount(),
                'selling_price' => $invoiceService->calculateSellingPrice($invoice),
                'due_on' => $invoice->getDueOn(),
                'created_at' => $invoice->getCreated()
            ];
        }
        $totalInvoicesAmount = $invoiceService->getTotalInvoicesAmount();

        $paginatedInvoices = $paginator->paginate(
          $data,
          $perPage,
            CommonEnum::INVOICES_PER_PAGE
        );

        return $this->render('invoices/index.html.twig', [
            'invoices' => $paginatedInvoices,
            'totalInvoicesAmount' => $totalInvoicesAmount
        ]);
    }

    /**
     * @Route (path="/invoices/create", name="invoices.create")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(): \Symfony\Component\HttpFoundation\Response
    {
        return $this->render('invoices/create.html.twig');
    }

    /**
     * @Route (path="/invoices/store", name="invoices.store")
     * @param Request $request
     * @param InvoiceService $invoiceService
     */
    public function store(Request $request, InvoiceService $invoiceService): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $file = $request->files->get('file');
        $uploadDirectory = $this->getParameter('uploadDirectory');

        $fileName = $invoiceService->uploadCSVFile($file);

        $csvPath = $uploadDirectory. '/' . $fileName;
        $csv = Reader::createFromPath($csvPath, 'r');
        $csv->setHeaderOffset(0);

        foreach ($csv as $key => $record) {
            $invoiceService->saveInvoice($record, $key++, $file->getClientOriginalName());
        }

        $this->addFlash('success', 'Invoices has been imported successfully.');
        return $this->redirect('/invoices');
    }
}