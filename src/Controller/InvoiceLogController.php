<?php

namespace App\Controller;

use App\Enum\CommonEnum;
use App\Service\InvoiceLogService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceLogController extends AbstractController
{
    /**
     * @Route(path="/invoices/logs", name="invoice.logs.index")
     * @param Request $request
     * @param InvoiceLogService $invoiceLogService
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllInvoiceLogs(
        Request $request,
        InvoiceLogService $invoiceLogService,
        PaginatorInterface $paginator
    ): \Symfony\Component\HttpFoundation\Response
    {
        $perPage = $request->query->get('page', CommonEnum::DEFAULT_PAGE);
        $invoiceLogs = $invoiceLogService->getAllInvoiceLogs();

        $invoiceLogs = $paginator->paginate(
            $invoiceLogs,
            $perPage,
            CommonEnum::INVOICES_PER_PAGE
        );

        return $this->render('invoice-logs/index.html.twig', [
           'invoiceLogs' => $invoiceLogs
        ]);
    }
}