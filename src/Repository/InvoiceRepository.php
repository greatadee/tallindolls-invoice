<?php

namespace App\Repository;

use App\Entity\Invoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class InvoiceRepository extends AbstractRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    /**
     * @return int|mixed|string
     */
    public function getAllInvoices()
    {
        return $this->findBy([], ['id' => 'DESC']);
    }

    /**
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalInvoicesAmount()
    {
        return $this->createQueryBuilder('inv')
            ->select('sum(inv.amount) as total_amount')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param array $data
     * @return Invoice
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(array $data): Invoice
    {
        $invoice = new Invoice();
        $invoice->setAmount($data['amount']);
        $invoice->setDueOn(new \DateTime($data['due on']));
        $invoice->setCreated(new \DateTime());
        $invoice->setUpdated(new \DateTime());

        $this->persist($invoice, true);
        return $invoice;
    }
}
