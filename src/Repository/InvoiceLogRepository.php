<?php

namespace App\Repository;

use App\Entity\InvoiceLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class InvoiceLogRepository extends AbstractRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvoiceLog::class);
    }

    /**
     * @param $data
     * @return InvoiceLog
     */
    public function create($data): InvoiceLog
    {
        $invoiceLog = new InvoiceLog();
        $invoiceLog->setFileName($data['file_name']);
        $invoiceLog->setMessage($data['message']);
        $invoiceLog->setCreated(new \DateTime());
        $invoiceLog->setUpdated(new \DateTime());

        $this->persist($invoiceLog, true);
        return $invoiceLog;
    }

    /**
     * @return array|object[]
     */
    public function getAllInvoiceLogs()
    {
        return $this->findBy([], ['id' => 'DESC']);
    }
}
