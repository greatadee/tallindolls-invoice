<?php

namespace App\Entity;

use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice extends AbstractEntity
{
    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="date")
     */
    private $dueOn;

    /**
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return $this
     */
    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDueOn(): ?\DateTimeInterface
    {
        return $this->dueOn;
    }

    /**
     * @param \DateTimeInterface $dueOn
     * @return $this
     */
    public function setDueOn(\DateTimeInterface $dueOn): self
    {
        $this->dueOn = $dueOn;

        return $this;
    }
}
